﻿using HtmlAgilityPack;
using MTScrapper.Model;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.Logging;

namespace MTScrapper.BAL
{
    public static class BALDataExtractor
    {

        public static List<FileModel> GetFiles()
        {
            FileModel fileModel = new FileModel();
            var directory = Path.Combine(Directory.GetCurrentDirectory(), "Uploads");
            //var uploadsFolder = Path.Combine(_environment.WebRootPath, "uploads");
            var fileNames = Directory.GetFiles(directory);

            return fileNames.Select(fn => new FileModel
            {
                FileName = Path.GetFileName(fn),
                DownloadLink = $"/Uploads/{Path.GetFileName(fn)}"
            }).ToList();
        }

        public static async Task<FileModel> Download(FileModel fileModel)
        {
            var provider = new FileExtensionContentTypeProvider();
            if (provider.TryGetContentType(fileModel.path, out var contentType))
            {
                contentType = "application/octet-stream";
            }

            var bytes = await System.IO.File.ReadAllBytesAsync(fileModel.path);
            fileModel.Bytes = bytes;
            fileModel.contentType = contentType;
            return fileModel;
        }
        public static List<ExtractData> ExtractDataFromHtml(HtmlDocument doc)
        {
            var extractedData = new List<ExtractData>();
            var orderedList = doc.DocumentNode.SelectSingleNode("//ol");
            if (orderedList != null)
            {
                foreach (var listItem in orderedList.Descendants("li"))
                {
                    var name = listItem.SelectSingleNode(".//a/span[@data-anonymize='person-name']");
                    var designation = listItem.SelectSingleNode(".//span[@data-anonymize='title']");
                    var Country = listItem.SelectSingleNode(".//span[@data-anonymize='location']");
                    var About = listItem.SelectSingleNode(".//dl/div/dd/div/span[2]");
                    var link = listItem.SelectSingleNode(".//a[@data-control-name=\"view_company_via_profile_lockup\"]");
                    var profile = listItem.SelectSingleNode(".//a[@data-control-name='view_lead_panel_via_search_lead_image']");
                    if (name == null || designation == null || link == null || Country == null || About ==null)
                    {
                        continue;
                    }

                    extractedData.Add(new ExtractData
                    {
                        Name = name.InnerText.Trim(),
                        Designation = designation.InnerText.Trim(),
                        WebsiteLink = link.Attributes["href"].Value,
                        Company = link.InnerText.Trim(),
                        About = About.InnerText.Trim(),
                        Country = Country.InnerText.Trim(),
                        ProfileURl = profile.Attributes["href"].Value
                    });
                }
            }
            return extractedData;
        }

        public static string CreateHtmlTable(List<ExtractData> extractedData)
        {
            var tableRows = extractedData.Select(data => $@"
                    <tr>
                        <td>{data.Name}</td>
                        <td>{data.Designation}</td>
                        <td>{data.WebsiteLink}</td>
                        <td>{data.Company}</td>
                        <td>{data.Country}</td>
                        <td>{data.ProfileURl}</td>
                        <td>{data.About}</td>
                    </tr>
                ");
            var htmlTable = $@"
                <html>
                    <head>
                        <style>table {{ width: 100%; border: solid 1px; }} th, td {{ width: 20%; height:70px; text-align: left; vertical-align:top}}</style>
                    </head>
                    <body>
                        <table>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Designation</th>
                                    <th>Company URL</th>
                                    <th>Company</th>
                                    <th>Country</th>
                                    <th>Profile URL</th>
                                    <th>About</th>
                                </tr>
                            </thead>
                            <tbody>
                                {string.Join("", tableRows)}
                            </tbody>
                        </table>
                    </body>
                </html>
            ";
            return htmlTable;
        }

        public static async Task<FileModel> SaveHtmlTableToFile(string htmlTable, string name)
        {

            FileModel fileModel = new FileModel();
            var directory = Path.Combine(Directory.GetCurrentDirectory(), "Uploads");
            Directory.CreateDirectory(directory);
            fileModel.path = Path.Combine(directory, name+$"_{DateTime.Now.Ticks}.xls");
            System.IO.File.WriteAllText(fileModel.path, htmlTable);
            //  return filePath;

            var provider = new FileExtensionContentTypeProvider();
            if (provider.TryGetContentType(fileModel.path, out var contentType))
            {
                contentType = "application/octet-stream";
            }

            var bytes = await System.IO.File.ReadAllBytesAsync(fileModel.path);
            fileModel.Bytes = bytes;
            fileModel.contentType = contentType;
            
            return fileModel;

        }
    }
}
