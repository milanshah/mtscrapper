﻿namespace MTScrapper.Model
{
    public class ExtractData
    {
        public string Name { get; set; }
        public string Designation { get; set; }
        public string WebsiteLink { get; set; }
        public string Company { get; set; }
        public string Country { get; set; }
        public string About { get; set; }
        public string ProfileURl { get; set; }

    }

}
