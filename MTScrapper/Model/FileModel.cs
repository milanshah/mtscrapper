﻿namespace MTScrapper.Model
{
    public class FileModel
    {
        public byte[] Bytes { get; set; }
        public string path { get; set; }
        public string contentType { get; set; }
        public string FileName { get; set; }
        public long Size { get; set; }
        public string DownloadLink { get; set; }
    }
}
