﻿using HtmlAgilityPack;
using MTScrapper.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.StaticFiles;
using OfficeOpenXml;
using System.Net.Mime;

namespace MTScrapper.Pages
{
    public class LinkedInScrapperModel : PageModel
    {
        private readonly ILogger<LinkedInScrapperModel> _logger;
        private readonly IConfiguration _configuration;
        public LinkedInScrapperModel(ILogger<LinkedInScrapperModel> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }
        public List<FileModel> Files { get; set; }
        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPostAsync()
        {
            // Get the uploaded HTML file
            var htmlFile = Request.Form.Files.GetFile("htmlFile");
            if (htmlFile == null)
            {
                return BadRequest("No file uploaded");
            }

            // Load the HTML data into an HtmlDocument
            var doc = new HtmlDocument();
            await using (var stream = htmlFile.OpenReadStream())
            {
                doc.Load(stream);
            }

            // Extract the required data from the HTML using XPath or other methods
            var extractedData = BAL.BALDataExtractor.ExtractDataFromHtml(doc);

            // Create a HTML table with the extracted data
            var htmlTable = BAL.BALDataExtractor.CreateHtmlTable(extractedData);

            // Save the HTML table to a file
            FileModel file = await BAL.BALDataExtractor.SaveHtmlTableToFile(htmlTable, Path.GetFileNameWithoutExtension(htmlFile.FileName));

            // Return the file as a downloadable attachment
            return File(file.Bytes, file.contentType, Path.GetFileName(file.path));

        }


    }
}